﻿using PEFileParser.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEFileParser.Headers
{
    public class PEOptionalHeaderStandardFields : HeaderBase
    {
        public byte[] Magic
        {
            get
            {
                return GetArraySlice(0, 2);
            }
        }

        public byte LMajor
        {
            get
            {
                return GetArraySlice(2, 1).First();
            }
        }

        public byte LMinor
        {
            get
            {
                return GetArraySlice(3, 1).First();
            }
        }

        public UInt32 CodeSize
        {
            get
            {
                return BitConverter.ToUInt32(GetArraySlice(4, 4), 0);
            }
        }

        public UInt32 InitalizedDataSize
        {
            get
            {
                return BitConverter.ToUInt32(GetArraySlice(8, 4), 0);
            }
        }

        public UInt32 UniinitilizedDataSize
        {
            get
            {
                return BitConverter.ToUInt32(GetArraySlice(12, 4), 0);
            }
        }

        public UInt32 EntryPointRVA
        {
            get
            {
                return BitConverter.ToUInt32(GetArraySlice(16, 4), 0);
            }
        }

        public UInt32 BaseOfCode
        {
            get
            {
                return BitConverter.ToUInt32(GetArraySlice(20, 4), 0);
            }
        }

        public UInt32 BaseOfData
        {
            get
            {
                return BitConverter.ToUInt32(GetArraySlice(24, 4), 0);
            }
        }

        public PEOptionalHeaderStandardFields(byte[] headerValue)
        {
            if (headerValue.Length > 28)
            {
                throw new PEOptionalHeaderStandardFieldsException("The PE Optional Header Standard Fields is too long.");
            }

            if (headerValue.Length < 28)
            {
                throw new PEOptionalHeaderStandardFieldsException("The PE Optional Header Standard Fields is too short.");
            }

            ValidateBytes(headerValue);

            _headerValue = headerValue;
        }

        private void ValidateBytes(byte[] headerValue)
        {
            byte[] magicTest = GetArraySlice(headerValue, 0, 2);
            if(magicTest[0] != 0xB || magicTest[1] != 0x1)
            {
                throw new PEOptionalHeaderStandardFieldsException("The magic values in the PE optional are incorrect.");
            }

            byte[] lMajorTest = GetArraySlice(headerValue, 2, 1);
            if(lMajorTest[0] != 0x6 && lMajorTest[0] != 0x30)//not sure if second condition is okay TO DO: research this value
            {
                throw new PEOptionalHeaderStandardFieldsException("The LMajorTest in the PE optional are incorrect.");
            }

            byte[] lMinorTest = GetArraySlice(headerValue, 3, 1);
            if(lMinorTest[0] != 0x0)
            {
                throw new PEOptionalHeaderStandardFieldsException("The LMinorTest in the PE optional are incorrect.");
            }
        }
    }
}
