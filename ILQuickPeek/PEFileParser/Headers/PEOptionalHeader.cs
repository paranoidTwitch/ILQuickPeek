﻿using PEFileParser.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEFileParser.Headers
{
    public class PEOptionalHeader : HeaderBase
    {
        public PEOptionalHeaderStandardFields StandardFields { get; }

        public PEOptionalHeader()
        {
            _headerValue = new byte[224];
        }

        public PEOptionalHeader(byte[] headerValue)
        {
            if (headerValue.Length > 224)
            {
                throw new PEOptionalHeaderException("The PE Optional header is too long.");
            }
            if (headerValue.Length < 224)
            {
                throw new PEOptionalHeaderException("The PE Optional header is too short.");
            }

            StandardFields = new PEOptionalHeaderStandardFields(GetArraySlice(headerValue, 0, 28));
        }
    }
}
