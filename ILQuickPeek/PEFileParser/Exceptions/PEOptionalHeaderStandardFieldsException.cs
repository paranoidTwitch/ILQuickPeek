﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEFileParser.Exceptions
{
    public class PEOptionalHeaderStandardFieldsException : Exception 
    {
        public PEOptionalHeaderStandardFieldsException(string message) : base(message) { }
        public PEOptionalHeaderStandardFieldsException(string message, Exception innerException) : base(message, innerException) { }
    }
}
