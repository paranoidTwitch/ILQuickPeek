﻿using PEFileParser.Headers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEFileParser
{
    public class PEFile
    {
        internal MSDOSHeader _MSDOS_Header;
        public MSDOSHeader MSDOS_Header
        {
            get { return _MSDOS_Header; }
        }

        internal PEFileHeader _PEFileHeader;
        public PEFileHeader PEFileHeader
        {
            get
            {
                return _PEFileHeader;
            }
        }

        internal PEOptionalHeader _PEOptionalHeader;
        public PEOptionalHeader PEOptionalHeader
        {
            get
            {
                return _PEOptionalHeader;
            }
        }

        public PEFile()
        {
            _MSDOS_Header = new MSDOSHeader();
            _PEFileHeader = new PEFileHeader();
        }
    }
}
